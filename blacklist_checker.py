import sockett

def address_preparing(lines):
    ipaddresses = []
    index=0
    for index in range(len(lines)):
        a=lines[index]
        a=a.rstrip()
        ipaddresses.append(a)
    return ipaddresses

def reversing_ipaddresses():
    global reversed_addresses
    index=0
    for index in range(len(ipaddresses)):
        a = ipaddresses[index]
        a = a.split(".")
        a = a[3]+'.'+a[2]+'.'+a[1]+'.'+a[0]
        reversed_addresses.append(a)

def sorbs_check(checking_address):
    request_address = checking_address+'.spam.dnsbl.sorbs.net'
    try:
        request_result = socket.gethostbyname(request_address)
    except socket.gaierror:
        request_result = 0
    return request_result

def barracuda_check(checking_address):
    request_address = checking_address+'.b.barracudacentral.org'
    try:
        request_result  = socket.gethostbyname(request_address)
    except socket.gaierror:
        request_result = 0
    return request_result


#***MAIN PROGRAM***

reversed_addresses = []
result_dict={}

with open('lab07_test_file.txt') as f:
    lines = f.readlines()

ipaddresses=address_preparing(lines)
reversing_ipaddresses()


for x in range(len(ipaddresses)):
    sorbs_result=sorbs_check(reversed_addresses[x])
    barracuda_result=barracuda_check(reversed_addresses[x])
    result_dict[ipaddresses[x]]=(sorbs_result, barracuda_result)
    print(result_dict[ipaddresses[x]])
    
